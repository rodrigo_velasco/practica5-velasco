<html>
    <head>
        <title>Aplicacion para notificaciones</title>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <script src="https://www.gstatic.com/firebasejs/5.5.9/firebase.js"></script>
        <script>
            // Initialize Firebase
            var config =
            {
                apiKey: "AIzaSyB2pb5M7_ep1Uo62OFTeWcomctRRmDvRyA",
                authDomain: "practica5-velasco.firebaseapp.com",
                databaseURL: "https://practica5-velasco.firebaseio.com",
                projectId: "practica5-velasco",
                storageBucket: "practica5-velasco.appspot.com",
                messagingSenderId: "257213197074"
            };

            firebase.initializeApp(config);

            // Retrieve Firebase Messaging object.
            const messaging = firebase.messaging();

            messaging.usePublicVapidKey("BG6IV1lg1QvCArstMi7UlyecAM-gXXsdlqB574d-7YCk_nJ_hiWPGGjSzxCWPjlFdXBV4vN_33AIAtjSYaKrYlI ");

            messaging.requestPermission().then(function() {
            console.log('Notification permission granted.');
            // TODO(developer): Retrieve an Instance ID token for use with FCM.
            // ...
            }).catch(function(err) {
            console.log('Unable to get permission to notify.', err);
            });

            // Get Instance ID token. Initially this makes a network call, once retrieved
            // subsequent calls to getToken will return from cache.
            messaging.getToken().then(function(currentToken) {
            if (currentToken) {
            sendTokenToServer(currentToken);
            updateUIForPushEnabled(currentToken);
            } else {
            // Show permission request.
            console.log('No Instance ID token available. Request permission to generate one.');
            // Show permission UI.
            updateUIForPushPermissionRequired();
            setTokenSentToServer(false);
            }
            }).catch(function(err) {
            console.log('An error occurred while retrieving token. ', err);
            showToken('Error retrieving Instance ID token. ', err);
            setTokenSentToServer(false);
            });
            

        </script>

        <link rel="manifest" href="manifest.json">
    </head>
    
    <body>
    <h1>Aplicacion para notificaciones</h1>
    </body>
</html>
